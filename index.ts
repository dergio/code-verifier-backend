import dotenv from 'dotenv';
import server from './src/server';
import { logError, logSuccess } from './src/utils/logger';

// Load environment variables
dotenv.config();

let port: string | number = process.env.PORT || 3000;

// Start server (listen)
server.listen(port, () => {
    logSuccess(`Server running /api on port ${port}`);
});

// Server Error Handler
server.on("error", (error) => {
    logError(`Error starting server: ${error}`);
});
