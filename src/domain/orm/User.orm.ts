import { userEntity } from "../entities/User.entity";
import { logSuccess, logError } from "src/utils/logger";

/**
 * This function is responsible for creating a new user in the database.
 * @param userData 
 * @returns Promise<any | undefined>
 */
export const createUser = async (userData: any): Promise<any | undefined> => {
    try {
        const User = userEntity();
        const newUser = new User(userData);
        await newUser.save();
        logSuccess(`User ${userData.name} created successfully`);
        return newUser;
    } catch (error) {
        logError(`Error creating user: ${error}`);
        return null;
    }
}

/**
 * This method is responsible for retrieving all users from the database.
 * @returns Promise<any[] | null>
 */ 
export const getUsers = async (): Promise<any[] | null> => {
    try {
        const User = userEntity();
        const users = await User.find({isDeleted: false});
        logSuccess(`Users retrieved successfully`);
        return users;
    } catch (error) {
        logError(`Error retrieving users: ${error}`);
        return null;
    }
}

/**
 * This method is responsible for retrieving a user by their ID.
 * @param id
 * @returns Promise<any | null>
 */
export const getUserById = async (id: string): Promise<any | null> => {
    try {
        const User = userEntity();
        const user = await User.findById(id);
        logSuccess(`User retrieved successfully`);
        return user;
    } catch (error) {
        logError(`Error retrieving user: ${error}`);
        return null;
    }
}

/**
 * This method is responsible for retrieving a user by their ID.
 * @param userEmail
 * @returns Promise<any | null>
 */
export const getUserByEmail = async (userEmail: string): Promise<any | null> => {
    try {
        const User = userEntity();
        const user = await User.find({email: userEmail});
        logSuccess(`User retrieved successfully`);
        return user;
    } catch (error) {
        logError(`Error retrieving user: ${error}`);
        return null;
    }
}

/**
 * This method is responsible for updating a user by their ID.
 * @param id
 * @param userData
 * @returns Promise<any | null>
 */
export const updateUser = async (id: string, userData: any): Promise<any | null> => {
    try {
        const User = userEntity();
        const user = await User.findByIdAndUpdate(id, userData, {new: true});
        logSuccess(`User updated successfully`);
        return user;
    } catch (error) {
        logError(`Error updating user: ${error}`);
        return null;
    }
}

/**
 * This method is responsible for deleting a user by their ID.
 * @param id
 * @returns Promise<any | null>
 */
export const deleteUser = async (id: string): Promise<any | null> => {
    try {
        const User = userEntity();
        const user = await User.findByIdAndDelete(id);
        logSuccess(`User deleted successfully`);
        return user;
    } catch (error) {
        logError(`Error deleting user: ${error}`);
        return null;
    }
}