import mongoose from "mongoose";

export const userEntity = () => {
    let userSchema = new mongoose.Schema({
        name: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true,
            unique: true
        },
        age: {
            type: Number,
            required: false
        }
    });

    return mongoose.model('User', userSchema);
}