import { Get, Query, Route, Tags } from "tsoa"
import { BasicResponse } from "./types";
import { IHelloController } from "./interfaces";
import { logSuccess } from "../utils/logger";

@Route("/api/hello")
@Tags("HelloController")
export class HelloController implements IHelloController {
    /**
     * Endpoint to return a greeting message "Hello {name}!" in JSON format
     * @param { string | undefined} name Name of user to be greeted
     * @returns { BasicResponse } JSON response with message from server promise
     */
    @Get("/")
    public async getMessage(@Query() name?: string): Promise<BasicResponse> {
        logSuccess(`[/api/hello] Get request Hello ${name || "World"}!`);
        return { 
            message: `Hello ${name || "World"}!` 
        };
    }
}

