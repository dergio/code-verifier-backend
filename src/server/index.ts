import express, { Express, Request, Response } from "express";

// Swagger
// import swaggerUi from "swagger-ui-express";
// import swaggerDocument from "swagger.json"; // Ruta a tu archivo swagger.json

// Security
import cors from "cors";
import helmet from "helmet";
// TODO: https

// Routes
import rootRouter from "../routes";

// Initialize express
const server: Express = express();
// const port: string | number = process.env.PORT || 3000;

// Define routes SERVER to use "/api" as prefix
server.use("/api", rootRouter);

// Static server
server.use(express.static('public'));

// Swagger
// server.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// TODO: Mongoose connection

// Middlewares for security
server.use(helmet());
server.use(cors());

// Content type configuration
server.use(express.urlencoded({ extended: true, limit: "50mb" }));
server.use(express.json({ limit: "50mb" }));

// Redirection site root url to "/api"
server.get("/", (req: Request, res: Response) => {
    res.redirect("/api");
});

export default server;