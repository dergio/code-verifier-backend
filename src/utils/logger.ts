export const logInfo = ( message: string ) => {
    console.log(`Info: %c${message}`);
}

export const logSuccess = ( message: string ) => {
    console.log(`Success: %c${message}`);
}

export const logWarning = ( message: string ) => {
    console.log(`Warning: %c${message}`);
}

export const logError = ( message: string ) => {
    console.log(`Error: %c${message}`);
}
