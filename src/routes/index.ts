/**
 * Root Router
 * Redirections to other routers
 */

import express, { Request, Response } from "express";
import HelloRouter  from "./HelloRouter";
import { logInfo } from "../utils/logger";

// Server instance
const server = express();

// Root Router
const rootRouter = express.Router();

// Redirect to /api/
rootRouter.get('/', (req: Request, res: Response) => {
    logInfo(`[/] Redirect to /api`);
    return res.send('Welcome to api restful');
});

/**
 * Register Routers [redirections to other routers and Controllers]
 */
// Redirect to /api
server.use('/', rootRouter);
// Redirect to /api/hello
server.use('/hello', HelloRouter);
// TODO: Add more routers here

// Export the server
export default server;

