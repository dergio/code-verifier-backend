import express, { Request, Response } from 'express';
import { HelloController } from '../controller/HelloController';
import { logInfo } from '../utils/logger';

const helloRouter = express.Router();

helloRouter.route('/')
    .get(async (req: Request, res: Response) => {
        // Obtain a query params from url /hello?name=John
        let name: string = req.query.name as string;
        logInfo(`[/api/hello] Query param ${name}`);
        
        const controller: HelloController = new HelloController();
        const response = await controller.getMessage(name);
        return res.send(response);
    });

// Export the router
export default helloRouter;